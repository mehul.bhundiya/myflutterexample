import 'package:flutter/material.dart';

class CustomPageView extends StatelessWidget {
  CustomPageView({super.key});

  PageController _controller = PageController();
  int selectedPageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Custom Page View'),
      ),
      body: PageView(
        controller: _controller,
        scrollDirection: Axis.horizontal,
        physics: NeverScrollableScrollPhysics(),
        onPageChanged: (value) {
          selectedPageIndex = value;
        },
        children: [
          ElevatedButton(
            onPressed: () {
              _controller.jumpToPage(selectedPageIndex);
            },
            child: Container(
              color: Colors.red,
              child: Center(
                child: Text(
                  'Page 0',
                  style: TextStyle(color: Colors.white, fontSize: 50),
                ),
              ),
            ),
          ),
          Container(
            color: Colors.blue,
            child: Center(
              child: Text(
                'Page 1',
                style: TextStyle(color: Colors.white, fontSize: 50),
              ),
            ),
          ),
          Container(
            color: Colors.black,
            child: Center(
              child: Text(
                'Page 2',
                style: TextStyle(color: Colors.white, fontSize: 50),
              ),
            ),
          ),
          Container(
            color: Colors.orange,
            child: Center(
              child: Text(
                'Page 3',
                style: TextStyle(color: Colors.white, fontSize: 50),
              ),
            ),
          ),
          Container(
            color: Colors.green,
            child: Center(
              child: Text(
                'Page 4',
                style: TextStyle(color: Colors.white, fontSize: 50),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
