class DataFunction {
  int? a;
  int? b;

  void initDataToVariableAB({int? a, required int? b, required Function? abc}) {
    this.a = a;
    this.b = b;
    print('VALUE OF A : $a && B : $b');

    int c = a! + b!;
    abc!(ans: c);
  }
}
