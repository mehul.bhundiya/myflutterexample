import 'package:sqflite/sqflite.dart';

class MyDatabase {
  Future<Database> initDatabase() async {
    return await openDatabase(
      '${await getDatabasesPath()}/my_database.db',
      version: 1,
      onCreate: (db, version) async {
        await db.execute(
            'CREATE TABLE TBL_USER(UserID INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, City TEXT)');
      },
    );
  }

  Future<int> insertUserInTBLUser({required name, required city}) async {
    Database db = await initDatabase();
    Map<String, dynamic> map = {};
    map['Name'] = name;
    map['City'] = city;
    return await db.insert('TBL_USER', map);
  }

  Future<int> updateUserInTblUser(
      {required name, required city, required id}) async {
    Database db = await initDatabase();
    Map<String, dynamic> map = {};
    map['Name'] = name;
    map['City'] = city;
    // UPDATE TBL_USER SET Name = name AND City = city WHERE UserID = id
    return await db
        .update('TBL_USER', map, where: 'UserID = ?', whereArgs: [id]);
  }

  Future<List<Map<String, Object?>>> getUserListFromTBLUser() async {
    return await (await initDatabase()).rawQuery('SELECT * FROM TBL_USER');
  }

  Future<int> deleteUserFromTblUser({required id}) async {
    return await (await initDatabase())
        .delete('TBL_USER', where: 'UserID = ?', whereArgs: [id]);
  }
}
