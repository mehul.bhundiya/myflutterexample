import 'package:flutter/material.dart';
import 'package:my_flutter_example/app_constants.dart';

class DivideScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: const Text(
          'Divide Page',
          style: TextStyle(
            color: Colors.white,
            fontSize: 50,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: [
          PopupMenuButton(
            icon: Icon(Icons.filter_alt_sharp),
            iconSize: 25,
            iconColor: Colors.cyan,
            onSelected: (value) {
              if (value == MORE_APPS) {
                print(':::$value:::');
              }
            },
            itemBuilder: (context) {
              return [
                getActionWidget(
                  icon: Icons.supervised_user_circle,
                  title: ABOUT_US,
                ),
                getActionWidget(
                  icon: Icons.email,
                  title: CONTACT_US,
                  customColor: Colors.red,
                ),
                getActionWidget(
                  icon: Icons.apps,
                  title: MORE_APPS,
                  customColor: Colors.blue,
                ),
                getActionWidget(
                  icon: Icons.share,
                  title: SHARE_APPS,
                  customColor: Colors.green,
                ),
              ];
            },
          ),
        ],
      ),
      body: Row(
        children: [
          Expanded(
            flex: 2,
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.asset(
                  'assets/images/3408.webp',
                  fit: BoxFit.cover,
                ),
                Center(
                  child: Text(
                    'DOG IS HONEST ANIMAL',
                    style: TextStyle(
                      color: Colors.red,
                      fontSize: 50,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              color: Colors.black,
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              color: Colors.amber,
            ),
          ),
        ],
      ),
    );
  }

  PopupMenuItem getActionWidget({
    required IconData icon,
    required String title,
    Color? customColor,
  }) {
    return PopupMenuItem(
      value: title,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          children: [
            Icon(
              icon,
              size: 18,
              color: customColor ?? Colors.black,
            ),
            SizedBox(
              width: 7,
            ),
            Text(
              title,
              style: TextStyle(color: customColor ?? Colors.black),
            ),
          ],
        ),
      ),
    );
  }
}
