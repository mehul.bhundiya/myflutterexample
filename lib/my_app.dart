import 'package:flutter/material.dart';
import 'package:my_flutter_example/bca_session/insert_user_page.dart';
import 'package:my_flutter_example/bca_session/user_list_page.dart';
import 'package:my_flutter_example/db_access/my_database.dart';
import 'package:my_flutter_example/my_db/my_db.dart';
import 'package:my_flutter_example/shared_preference_demo.dart';
import 'package:my_flutter_example/user_list_crud/user_list_page.dart';

class MyApp extends StatelessWidget {
  MyApp({super.key});

  MyDatabase db = MyDatabase();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.red),
        useMaterial3: true,
      ),
      home: FutureBuilder(
        future: db.initDatabase(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return UserListPageBCA();
          } else {
            return CircularProgressIndicator();
          }
        },
      ),

      // FutureBuilder(
      //   future: MyDB().initDatabase(),
      //   builder: (context, snapshot) {
      //     if (snapshot.hasData) {
      //       return UserListPage();
      //     } else {
      //       return CircularProgressIndicator();
      //     }
      //   },
      // ),
    );
  }
}
