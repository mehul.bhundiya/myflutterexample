import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_example/app_constants.dart';
import 'package:my_flutter_example/future_function.dart';
import 'package:my_flutter_example/my_db/my_db.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:my_flutter_example/user_list_crud/user_entry_page.dart';

class UserListPage extends StatefulWidget {
  const UserListPage({super.key});

  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  List<dynamic> userList = [];
  List<dynamic> searchList = [];
  MyDB db = MyDB();
  bool isGetData = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'User List',
            style: TextStyle(
              color: Colors.black,
              fontSize: 30,
              fontWeight: FontWeight.bold,
            ),
          ),
          actions: [
            InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return UserEntryPage();
                    },
                  ),
                ).then(
                  (value) {
                    setState(
                      () {
                        isGetData = true;
                      },
                    );
                  },
                );
              },
              child: const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.add,
                  ),
                  Text(
                    'Add User',
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
            )
          ],
        ),
        body: FutureBuilder(
          future: getUserListFromApi(),
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data != null) {
              if (isGetData) {
                userList.addAll(snapshot.data!);
                searchList.addAll(userList);
                isGetData = false;
              }
              print('userList:::${userList.length}');
              return searchList.isEmpty
                  ? Center(
                      child: Text(
                        'No User Found',
                        style: TextStyle(
                          color: Colors.grey.shade400,
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                        ),
                      ),
                    )
                  : Column(
                      children: [
                        TextFormField(
                          onChanged: (value) {
                            searchList.clear();
                            for (int i = 0; i < userList.length; i++) {
                              if (userList[i]['name']
                                  .toString()
                                  .toLowerCase()
                                  .contains(value.toLowerCase())) {
                                searchList.add(userList[i]);
                              }
                            }
                            // setState(() {});
                          },
                        ),
                        Expanded(
                          child: ListView.builder(
                            itemBuilder: (context, index) {
                              return Card(
                                elevation: 10,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              searchList[index]['name'],
                                              style: const TextStyle(
                                                color: Colors.black,
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            Text(
                                              searchList[index]['City'],
                                              style: const TextStyle(
                                                color: Colors.black,
                                                fontSize: 12,
                                                fontWeight: FontWeight.normal,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      IconButton(
                                        onPressed: () {
                                          Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (context) {
                                                return UserEntryPage(
                                                  userDetail: searchList[index],
                                                );
                                              },
                                            ),
                                          ).then(
                                            (value) {
                                              // setState(
                                              //   () {
                                              //     searchList[index] = value;
                                              //   },
                                              // );
                                            },
                                          );
                                        },
                                        icon: const Icon(Icons.edit),
                                      ),
                                      IconButton(
                                        onPressed: () {
                                          showDialog(
                                            context: context,
                                            builder: (context) {
                                              return CupertinoAlertDialog(
                                                title: Text(
                                                    'Are you sure want to delete?'),
                                                actions: [
                                                  TextButton(
                                                    onPressed: () async {
                                                      bool isDeleted =
                                                          await deleteUserWithDELETEApi(
                                                              searchList[index]
                                                                  ['id']);
                                                      if (isDeleted) {
                                                        setState(() {
                                                          isGetData = true;
                                                          userList.clear();
                                                          searchList.clear();
                                                        });
                                                        Navigator.pop(context);
                                                      }
                                                    },
                                                    child: Text('Yes'),
                                                  ),
                                                  TextButton(
                                                    onPressed: () {},
                                                    child: Text('No'),
                                                  ),
                                                ],
                                              );
                                            },
                                          );
                                        },
                                        icon: Icon(
                                          Icons.delete,
                                          color: Colors.red,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                            itemCount: searchList.length,
                          ),
                        ),
                      ],
                    );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ));
  }

  Future<dynamic> getUserListFromApi() async {
    var response = await http
        .get(Uri.parse('https://638029948efcfcedacfe0228.mockapi.io/api/user'));
    return jsonDecode(response.body);
  }

  Future<bool> deleteUserWithDELETEApi(id) async {
    http.Response res = await http.delete(Uri.parse('$API_URL/$id'));
    return jsonDecode(res.body)['id'] == id;
  }
}
