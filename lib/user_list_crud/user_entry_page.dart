import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_example/app_constants.dart';
import 'package:my_flutter_example/my_db/my_db.dart';
import 'package:my_flutter_example/user_list_crud/city_entry_page.dart';
import 'package:http/http.dart' as http;

class UserEntryPage extends StatefulWidget {
  Map<String, dynamic>? userDetail = {};

  UserEntryPage({super.key, this.userDetail});

  @override
  State<UserEntryPage> createState() => _UserEntryPageState();
}

class _UserEntryPageState extends State<UserEntryPage> {
  GlobalKey<FormState> _formKey = GlobalKey();
  MyDB db = MyDB();
  TextEditingController _userNameController = TextEditingController();
  TextEditingController _cityController = TextEditingController();

  // List<Map<String, dynamic>> _cityList = [];
  // Map<String, dynamic> _cityDetail = {};

  @override
  void initState() {
    super.initState();
    if (widget.userDetail != null) {
      _userNameController.text = widget.userDetail!['name'];
      _cityController.text = widget.userDetail!['City'];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Add User'),
          actions: [
            IconButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) {
                    return CityEntryPage();
                  },
                ));
              },
              icon: Icon(Icons.add),
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  controller: _userNameController,
                  decoration: InputDecoration(
                    hintText: 'Enter User Name',
                    labelText: 'Enter User Name',
                  ),
                  style: TextStyle(fontSize: 20, color: Colors.black),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Enter User Name';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Enter City Name';
                    }
                    return null;
                  },
                  controller: _cityController,
                  decoration: InputDecoration(
                    hintText: 'Enter City Name',
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      Map<String, dynamic> map = {};
                      map['name'] = _userNameController.text.toString();
                      map['City'] = _cityController.text.toString();
                      http.Response res = (widget.userDetail == null)
                          ? await insertUserWithPOSTApi(map)
                          : await updateUserWithUPDATEApi(
                              map, widget.userDetail!['id']);
                      Navigator.of(context).pop();
                    }
                  },
                  child: Text('Submit'),
                ),
              ],
            ),
          ),
        ));
  }

  Future<http.Response> insertUserWithPOSTApi(map) async {
    return await http.post(Uri.parse(API_URL), body: map);
  }

  Future<http.Response> updateUserWithUPDATEApi(map, id) async {
    return await http.put(Uri.parse('$API_URL/$id'), body: map);
  }
}
