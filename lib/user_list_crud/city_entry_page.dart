import 'package:flutter/material.dart';
import 'package:my_flutter_example/my_db/my_db.dart';

class CityEntryPage extends StatelessWidget {
  TextEditingController _cityController = TextEditingController();
  GlobalKey<FormState> _formKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('City Detail'),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              TextFormField(
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter City Name';
                  }
                  return null;
                },
                controller: _cityController,
                decoration: InputDecoration(
                  hintText: 'Enter City Name',
                ),
              ),
              ElevatedButton(
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    Map<String, dynamic> map = {};
                    map[MyDB.CITY_NAME] = _cityController.text.toString();
                    int pk = await MyDB().insertCityInTblCity(map);
                    if (pk > 0) {
                      _cityController.text = '';
                    }
                  }
                },
                child: Text('Submit'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
