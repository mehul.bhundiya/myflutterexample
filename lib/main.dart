import 'package:flutter/material.dart';
import 'package:my_flutter_example/data_function.dart';
import 'package:my_flutter_example/my_app.dart';

void main() {
  runApp(MyApp());
  // ABC().abc();
}

class ABC extends DataFunction {
  abc() => initDataToVariableAB(
      b: 20,
      a: 30,
      abc: ({required int? ans}) {
        print("ADDITION OF TWO NUMBERS A & B :::: $ans");
      });
}
