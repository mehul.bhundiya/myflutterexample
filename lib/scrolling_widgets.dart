import 'package:flutter/material.dart';

class ScrollingPage extends StatefulWidget {
  @override
  State<ScrollingPage> createState() => _ScrollingPageState();
}

class _ScrollingPageState extends State<ScrollingPage> {
  List<String> names = [
    'Mehul',
    'Samir',
    'Sunil',
    'Jayesh',
    'Karan',
    'Het',
    'Jash',
    'Diyan',
    'Kiyan',
  ];

  bool isGridSelected = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ScrollingPage'),
        actions: [
          Container(
            color: isGridSelected ? Colors.transparent : Colors.blue,
            child: IconButton(
              onPressed: () {
                setState(() {
                  isGridSelected = false;
                });
              },
              icon: Icon(
                Icons.list,
                size: 30,
              ),
            ),
          ),
          Container(
            color: isGridSelected ? Colors.blue : Colors.transparent,
            child: IconButton(
              onPressed: () {
                setState(() {
                  isGridSelected = true;
                });
              },
              icon: Icon(
                Icons.grid_3x3,
                size: 30,
              ),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: isGridSelected
            ? GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3),
                itemBuilder: (context, index) {
                  return getListWidget(value: names[index]);
                },
                itemCount: names.length,
              )
            : ListView.builder(
                itemBuilder: (context, index) {
                  return getListWidget(value: names[index]);
                },
                itemCount: names.length,
              ),
      ),
    );
  }

  Widget getListWidget({required String value}) {
    return Card(
      elevation: 10,
      color: Colors.amber,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          value,
          style: const TextStyle(
            fontSize: 50,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
