import 'package:flutter/material.dart';
import 'package:my_flutter_example/db_access/my_database.dart';

class InsertUserPage extends StatefulWidget {
  Map<String, dynamic>? map = {};

  InsertUserPage({
    super.key,
    this.map,
  });

  @override
  State<InsertUserPage> createState() => _InsertUserPageState();
}

class _InsertUserPageState extends State<InsertUserPage> {
  GlobalKey<FormState> _formKey = GlobalKey();

  MyDatabase db = MyDatabase();

  TextEditingController nameController = TextEditingController();
  Map<String, dynamic>? selectedValue;
  TextEditingController cityController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.map != null) {
      nameController.text = widget.map!['Name'];
      cityController.text = widget.map!['City'];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          'Insert User',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              TextFormField(
                controller: nameController,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter Valid Name';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  labelText: 'Name',
                  hintText: 'Enter User Name',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      50,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: cityController,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter Valid City';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  labelText: 'City',
                  hintText: 'Enter City Name',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      50,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              FutureBuilder(
                future: db.getUserListFromTBLUser(),
                builder: (context, snapshot) {
                  if (snapshot.hasData && snapshot.data != null) {
                    selectedValue ??= snapshot.data![0];
                    print('VALUE ::: $selectedValue');
                    return DropdownButton(
                      items: snapshot.data!.map((e) {
                        return DropdownMenuItem(
                          child: Text(e['Name'].toString()),
                          value: e,
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          selectedValue = value;
                        });
                      },
                      value: selectedValue,
                    );
                  } else {
                    return Container();
                  }
                },
              ),
              SizedBox(
                height: 10,
              ),
              ElevatedButton(
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    int primaryKey = (widget.map == null)
                        ? await db.insertUserInTBLUser(
                            name: nameController.text.toString(),
                            city: cityController.text.toString(),
                          )
                        : await db.updateUserInTblUser(
                            name: nameController.text.toString(),
                            city: cityController.text.toString(),
                            id: widget.map!['UserID'],
                          );
                  }
                },
                child: Text('Save'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
