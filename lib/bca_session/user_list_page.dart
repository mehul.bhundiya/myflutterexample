import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:my_flutter_example/bca_session/insert_user_page.dart';
import 'package:my_flutter_example/db_access/my_database.dart';

class UserListPageBCA extends StatelessWidget {
  UserListPageBCA({super.key});

  MyDatabase db = MyDatabase();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'User List',
        ),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return InsertUserPage(
                      map: null,
                    );
                  },
                ),
              );
            },
            icon: Icon(Icons.add),
          ),
        ],
      ),
      body: FutureBuilder(
        future: db.getUserListFromTBLUser(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if(snapshot.data!=null && snapshot.data!.length>0){
              return ListView.builder(
                itemBuilder: (context, index) {
                  Map<String, dynamic> map = snapshot.data![index];
                  return InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return InsertUserPage(
                              map: map,
                            );
                          },
                        ),
                      );
                    },
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    map['Name'],
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    map['City'],
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            IconButton(
                              onPressed: () {
                                showAlert(context, map['UserID']);
                              },
                              icon: Icon(
                                Icons.delete,
                                color: Colors.red,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
                itemCount: snapshot.data!.length,
              );
            }else{
             return Center(child: Text('No Users Found',style: TextStyle(color: Colors.grey),));
            }

          } else {
            return CircularProgressIndicator();
          }
        },
      ),
    );
  }

  void showAlert(context, id) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text('Alert!'),
          content: Text('Are you sure want to delete this record?'),
          actions: [
            TextButton(
                onPressed: () async {
                  await db.deleteUserFromTblUser(id: id);
                  Navigator.of(context).pop();

                },
                child: Text('Yes')),
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('No'))
          ],
        );
      },
    );
  }
}
