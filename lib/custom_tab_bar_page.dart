import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomTabBarPage extends StatefulWidget {
  const CustomTabBarPage({super.key});

  @override
  State<CustomTabBarPage> createState() => _CustomTabBarPageState();
}

class _CustomTabBarPageState extends State<CustomTabBarPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Custom Tab Bar Page'),
      ),
      body: Column(
        children: [
          TabBar(
            indicator: BoxDecoration(
              color: Colors.green,
              border: Border.all(),
              borderRadius: BorderRadius.circular(20),
            ),
            indicatorSize: TabBarIndicatorSize.tab,
            controller: _tabController,
            tabs: [
              Text('Page 1'),
              Text('Page 2'),
              Text('Page 3'),
            ],
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: [
                Container(
                  color: Colors.blue,
                  child: Center(
                    child: Text(
                      'Page 1',
                      style: TextStyle(color: Colors.white, fontSize: 50),
                    ),
                  ),
                ),
                Container(
                  color: Colors.black,
                  child: Center(
                    child: Text(
                      'Page 2',
                      style: TextStyle(color: Colors.white, fontSize: 50),
                    ),
                  ),
                ),
                Container(
                  color: Colors.orange,
                  child: Center(
                    child: Text(
                      'Page 3',
                      style: TextStyle(color: Colors.white, fontSize: 50),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
