import 'package:flutter/material.dart';

class DrawerScreenPage extends StatefulWidget {
  const DrawerScreenPage({super.key});

  @override
  State<DrawerScreenPage> createState() => _DrawerScreenPageState();
}

class _DrawerScreenPageState extends State<DrawerScreenPage> {
  String value = 'Dashboard Page';
  int currentSelectedIndex = 0;
  dynamic backgroundColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: Text('Drawer Screen'),
      ),
      drawer: Drawer(
        child: Column(
          children: [
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.deepOrange),
              accountName: Text(
                'Mr. Mehul Bhundiya',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                ),
              ),
              accountEmail: Text(
                'mehul.bhundiya@darshan.ac.in',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
            ),
            ListTile(
              onTap: () {
                setState(() {
                  value = 'Home Page';
                  Navigator.of(context).pop();
                });
              },
              leading: Icon(Icons.home),
              title: Text('Home'),
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              onTap: () {
                setState(() {
                  value = 'Cart Page';
                  Navigator.of(context).pop();
                });
              },
              leading: Icon(Icons.shopping_cart),
              title: Text('Cart'),
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              onTap: () {
                setState(() {
                  value = 'CheckOut Page';
                  Navigator.of(context).pop();
                });
              },
              leading: Icon(Icons.checklist_outlined),
              title: Text('CheckOut'),
            ),
            Spacer(),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text('Logout'),
            ),
            Text('V2.8'),
          ],
        ),
      ),
      endDrawer: Drawer(
        child: Column(
          children: [
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.deepOrange),
              accountName: Text(
                'Mr. Mehul Bhundiya',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 25),
              ),
              accountEmail: Text(
                'mehul.bhundiya@darshan.ac.in',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
            ListTile(
              onTap: () {
                setState(() {
                  value = 'Home Page';
                  Navigator.of(context).pop();
                });
              },
              leading: Icon(Icons.home),
              title: Text('Home'),
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              onTap: () {
                setState(() {
                  value = 'Cart Page';
                  Navigator.of(context).pop();
                });
              },
              leading: Icon(Icons.shopping_cart),
              title: Text('Cart'),
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              onTap: () {
                setState(() {
                  value = 'CheckOut Page';
                  Navigator.of(context).pop();
                });
              },
              leading: Icon(Icons.checklist_outlined),
              title: Text('CheckOut'),
            ),
            Spacer(),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text('Logout'),
            ),
            Text('V2.8'),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            label: 'Cart',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.checklist_outlined),
            label: 'Check Out',
          ),
        ],
        currentIndex: currentSelectedIndex,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.amber,
        onTap: (value) {
          setState(() {
            currentSelectedIndex = value;
            if (currentSelectedIndex == 0) {
              backgroundColor = Colors.blue;
              this.value = 'Home Page';
            } else if (currentSelectedIndex == 1) {
              this.value = 'Cart Page';
              backgroundColor = Colors.red;
            } else {
              this.value = 'Checkout Page';
              backgroundColor = Colors.amber;
            }
          });
        },
      ),
      body: Center(
        child: Text(
          value,
          style: TextStyle(
              color: Colors.black, fontSize: 50, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
