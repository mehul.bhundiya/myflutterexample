import 'package:sqflite/sqflite.dart';

class MyDB {
  static const int DB_VERSION = 2;

  static const String TBL_CITY = 'Tbl_City';
  static const String CITY_ID = 'CityID';
  static const String CITY_NAME = 'CityName';

  static const String TBL_USER = 'Tbl_User';
  static const String USER_ID = 'UserID';
  static const String NAME = 'Name';

  Future<Database> initDatabase() async {
    String databasePath = await getDatabasesPath();
    return await openDatabase(
      '$databasePath/mydb.db',
      version: DB_VERSION,
      onCreate: (db, version) async {
        // await db.execute(
        //     'CREATE TABLE $TBL_USER($USER_ID INTEGER PRIMARY KEY AUTOINCREMENT, $NAME TEXT, $CITY TEXT)');
      },
      onUpgrade: (db, oldVersion, newVersion) async {
        if (newVersion > oldVersion) {
          await db.execute('DROP TABLE $TBL_USER');
          await db.execute(
              'CREATE TABLE $TBL_CITY($CITY_ID INTEGER PRIMARY KEY AUTOINCREMENT, $CITY_NAME TEXT)');
          await db.execute(
              'CREATE TABLE $TBL_USER($USER_ID INTEGER PRIMARY KEY AUTOINCREMENT, $NAME TEXT, $CITY_ID INTEGER REFERENCES $TBL_CITY($CITY_ID))');
        }
      },
    );
  }

  Future<int> insertCityInTblCity(map) async {
    return await (await initDatabase()).insert(TBL_CITY, map);
  }

  Future<int> insertUserInTblUser(map) async {
    return await (await initDatabase()).insert(TBL_USER, map);
  }

  Future<int> updateUserInTblUser(map, userID) async {
    return await (await initDatabase())
        .update(TBL_USER, map, where: '$USER_ID = ?', whereArgs: [userID]);
  }

  Future<int> deleteUserFromTblUser(userID) async {
    return await (await initDatabase())
        .delete(TBL_USER, where: '$USER_ID = ?', whereArgs: [userID]);
  }

  Future<List<Map<String, dynamic>>> getUserListFromTblUser() async {
    return (await initDatabase()).rawQuery(
        'SELECT * FROM $TBL_USER U INNER JOIN $TBL_CITY C ON C.$CITY_ID = U.$CITY_ID');
  }

  Future<List<Map<String, dynamic>>> getCityListFromTblCity() async {
    return (await initDatabase()).rawQuery('SELECT * FROM $TBL_CITY');
  }
}
