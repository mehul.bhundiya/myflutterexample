import 'package:flutter/material.dart';
import 'package:my_flutter_example/user_list_crud/user_entry_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceDemo extends StatefulWidget {
  SharedPreferenceDemo({super.key});

  @override
  State<SharedPreferenceDemo> createState() => _SharedPreferenceDemoState();
}

class _SharedPreferenceDemoState extends State<SharedPreferenceDemo> {
  SharedPreferences? _preferences;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return UserEntryPage();
                  },
                ),
              ).then((value) async {
                if (value != null) {
                  await _preferences!.setString('UserName', value['UserName']);
                  await _preferences!.setString('City', value['City']);
                  setState(() {});
                }
              });
            },
            icon: Icon(Icons.add),
          ),
        ],
      ),
      body: Column(
        children: [
          FutureBuilder<SharedPreferences>(
            future: SharedPreferences.getInstance(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                _preferences = snapshot.data;
                return Center(
                  child: Text(
                    'DATA HAS BEEN INITIALIZED : ${snapshot.data!.getString('UserName')}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 20,
                    ),
                  ),
                );
              } else if (snapshot.hasError) {
                return Text('ERROR HAS OCCURED : ${snapshot.error.toString()}');
              } else {
                return CircularProgressIndicator();
              }
            },
          ),
        ],
      ),
    );
  }
}
