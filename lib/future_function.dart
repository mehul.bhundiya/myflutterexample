import 'package:flutter/material.dart';

class FutureExplanation {
  int? noOfPizza;

  void orderPizza({required int noOfPizza, int? sauses, bool? extraCheese}) {
    this.noOfPizza = noOfPizza;
    print('Pizza Order No : $noOfPizza');
  }

  Future<void> preparePizza() async {
    return Future.delayed(
      Duration(seconds: 5),
      () {
        print('Your Order is Ready');
      },
    );
  }
}
